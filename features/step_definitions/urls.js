const baseUrl = 'http://localhost:3000';

module.exports = {
  loginUrl: baseUrl + '/login',
  registerUrl: baseUrl + '/register'
};